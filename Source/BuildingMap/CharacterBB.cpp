﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "CharacterBB.h"

#include "GameFramework/PawnMovementComponent.h"


// Sets default values
ACharacterBB::ACharacterBB()
{
	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void ACharacterBB::BeginPlay()
{
	Super::BeginPlay();
	if (GetMovementComponent()) GetMovementComponent()->GetNavAgentPropertiesRef().bCanCrouch = true;
}

void ACharacterBB::Crouch(bool bClientSimulation)
{
	Super::Crouch(bClientSimulation);
}

// Called every frame
void ACharacterBB::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ACharacterBB::ToggleRunning()
{
	// SetRunning(!bIsRunning);
}

void ACharacterBB::PsiBlast()
{
	// // The cost of the psi blast is 150.0f
	// // Check we have atleast that before allowing the function to work
	// if (CurrentPsiPower >= PsiBlastCost)
	// {
	// 	// Do the Psi Blast
	//
	// 	// Deduct the power used
	// 	CurrentPsiPower -= PsiBlastCost;
	// }
}


// Called to bind functionality to input
void ACharacterBB::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
}

