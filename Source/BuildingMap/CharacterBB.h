﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "CharacterBB.generated.h"

UCLASS()
class BUILDINGMAP_API ACharacterBB : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ACharacterBB();

	virtual void Crouch(bool bClientSimulation = false) override;

	
	UFUNCTION(BlueprintCallable, Category="Player|Movement")
	void ToggleRunning();
	
	// Player unleashes a devastating blast of mind power!
	UFUNCTION(BlueprintCallable, Category="Player|PsiPower")
	void PsiBlast();
	
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
};
